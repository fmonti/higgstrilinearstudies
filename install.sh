#!/bin/sh
#cd ~fmonti/work/flashggNew/CMSSW_10_5_0/
#eval `scramv1 runtime -sh`
#cd -

#install lhapdf
workdir=$PWD
wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.1.6.tar.gz -O LHAPDF-6.1.6.tar.gz
tar xf LHAPDF-6.1.6.tar.gz
rm LHAPDF-6.1.6.tar.gz
mkdir lhapdf_build/
cd LHAPDF-6.1.6
./configure --prefix=$PWD/../lhapdf_build/
make
make install


#download pdf set PDF4LHC15
wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/PDF4LHC15_nlo_mc.tar.gz -O- | tar xz -C $PWD/../lhapdf_build/share/LHAPDF/
cd ..

#make setup script
#echo 'cd ~fmonti/work/flashggNew/CMSSW_10_5_0/' >> ./setup.sh
#echo 'eval `scramv1 runtime -sh`' >> ./setup.sh
#echo 'cd -' >> ./setup.sh
echo "MGPATH=$PWD" > ./setup.sh
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MGPATH/lhapdf_build/lib/' >> ./setup.sh
echo 'export PATH=$MGPATH/lhapdf_build/bin:$PATH' >> ./setup.sh
echo 'export LD_LIBRARY_PATH=$MGPATH/lhapdf_build/lib:$LD_LIBRARY_PATH' >> ./setup.sh
echo 'export PYTHONPATH=$MGPATH/lhapdf_build/lib/python2.7/site-packages:$PYTHONPATH' >> ./setup.sh
echo 'export PYTHIA8DATA=$MGPATH/MG5_aMC_v2_6_7/HEPTools/pythia8/share/Pythia8/xmldoc/' >> ./setup.sh
source ./setup.sh

#download madgraph
wget https://launchpad.net/mg5amcnlo/2.0/2.5.x/+download/MG5_aMC_v2.5.5.tar.gz -O MG5_aMC_v2.5.5.tar.gz
tar xf MG5_aMC_v2.5.5.tar.gz
rm MG5_aMC_v2.5.5.tar.gz

#setup madgraph
cd MG5_aMC_v2_5_5/ 
echo "lhapdf = $PWD/../lhapdf_build/bin/lhapdf-config" >> input/mg5_configuration.txt
echo "automatic_html_opening = False" >> input/mg5_configuration.txt 
sed -i 's/# auto_update = 7/auto_update = 0/g' input/mg5_configuration.txt

#install mg5 packages
#NOTE collier should not be used. In order to avoid that MG5 installs it automatically at the first event generation, I will install it and then manually disable it.
./bin/mg5_aMC <<EOD
install pythia8
install mg5amc_py8_interface
install oneloop
install ninja
install collier
quit
EOD
sed -i 's&collier =.*&collier = None&g' input/mg5_configuration.txt
cd ..

#download and extract the software for the kappa lambda NLO reweighting
wget --no-check-certificate https://cp3.irmp.ucl.ac.be/projects/madgraph/raw-attachment/wiki/HiggsSelfCoupling/trilinear-RW.tar.gz 
tar xf trilinear-RW.tar.gz
rm trilinear-RW.tar.gz

