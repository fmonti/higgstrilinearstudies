#!/bin/python
import os,sys,copy,math
from optparse import OptionParser

process_dict={
    "ZH":  ["p p > h z [LOonly= QCD]"],
    "WH":  ["p p > h w+ [LOonly= QCD]","p p > h w- [LOonly= QCD]"],
    "ttH": ["p p > t t~ h [LOonly= QCD]"],
    "tHq": ["p p > h t j \$\$ w+ w- [LOonly= QCD]", "p p > h t~ j \$\$ w+ w- [LOonly= QCD]"]
}

scale_dict={
    "ZH":  108.0938,
    "WH":  102.6925,
    "ttH": 235.0,
    "tHq": 148.75
}   

workdir=os.getcwd()
parser = OptionParser()
parser.add_option("--proc",                 default="",         help="Process (ZH, WH, ttH, or tHq)")
parser.add_option("--label",                default="",         help="Label")
parser.add_option("--Nevents", type="int",  default=1000,       help="N events to generate")
parser.add_option("--dummy_process_dir", default=workdir+"/MG5_aMC_v2_5_5/test_ttbar/", help="Example mg5 generation from which we will copy some data and lib")
parser.add_option("--trilinear_dir",     default=workdir+"/trilinear-RW/",              help="Folder containing trilinear rew. code")
parser.add_option("--mg5_dir",           default=workdir+"/MG5_aMC_v2_5_5/",            help="MG5 folder")
parser.add_option("--LHAPDF_dir",        default=workdir+"/LHAPDF-6.1.6/",              help="LHAPDF folder")
parser.add_option("--store_dir",         default=workdir+"/",                           help="folder to store the output generated events")
(options,args)=parser.parse_args()

os.system("mkdir %s/%s/"%(options.store_dir,options.label))
os.system("cp -r %s/hhh-model %s/models/"%(options.trilinear_dir,options.mg5_dir))
os.system("cp %s/gevirt.sh %s/"%(options.trilinear_dir,options.mg5_dir))
os.chdir(options.mg5_dir)

mg5script = open("runmg5MC.sh","w")
mg5script.write("./bin/mg5_aMC <<EOF\n")
mg5script.write("import model hhh-model\n")
mg5script.write("generate %s\n"%process_dict[options.proc][0])
if len(process_dict[options.proc])>1:
    for iproc in range(1,len(process_dict[options.proc])):
        mg5script.write("add  process %s\n"%process_dict[options.proc][iproc])
mg5script.write("output %s_MC\n"%options.label)
mg5script.write("quit\n")
mg5script.write("EOF\n")
mg5script.close()

os.system("chmod u+x runmg5MC.sh")
print "generating born diagrams - see  %s/%s/runmg5MC.log for log"%(options.store_dir,options.label)
os.system("./runmg5MC.sh >  %s/%s/runmg5MC.log"%(options.store_dir,options.label))
os.system("./gevirt.sh %s_MC"%options.label)
os.system("echo './bin/mg5_aMC <<EOF' > runmg5ME.sh")
os.system("echo 'import model hhh-model' >> runmg5ME.sh")
os.system("cat proc_ml >> runmg5ME.sh")
os.system("echo 'output %s_ME' >> runmg5ME.sh"%options.label)
os.system("echo EOF >> runmg5ME.sh")
if options.proc == 'ttH':
    os.system("cp %s/tth-loop_diagram_generation.py %s/madgraph/loop/loop_diagram_generation.py"%(options.trilinear_dir,options.mg5_dir))
else:
    os.system("cp %s/vvh-loop_diagram_generation.py %s/madgraph/loop/loop_diagram_generation.py"%(options.trilinear_dir,options.mg5_dir))
os.system("chmod u+x runmg5ME.sh")
print "generating NLO EW diagrams - see  %s/%s/runmg5ME.log for log"%(options.store_dir,options.label)
os.system("./runmg5ME.sh >  %s/%s/runmg5ME.log"%(options.store_dir,options.label))

os.system("ls")
os.system("cp %s/makefile ./%s_ME/SubProcesses/"%(options.trilinear_dir,options.label))
os.system("cp %s/check_OLP.f ./%s_ME/SubProcesses/"%(options.trilinear_dir,options.label))
os.system("cp ./check_olp.inc ./%s_ME/SubProcesses/"%options.label)
os.system("cp ./%s_ME/SubProcesses/P0_*/pmass.inc ./%s_ME/SubProcesses/"%(options.label,options.label))
os.system("cp ./%s_ME/SubProcesses/P0_*/nsqso_born.inc ./%s_ME/SubProcesses/"%(options.label,options.label))    
os.system("cp ./%s_ME/SubProcesses/P0_*/nsquaredSO.inc ./%s_ME/SubProcesses/"%(options.label,options.label))
os.system("cp ./%s_MC/SubProcesses/c_weight.inc ./%s_ME/SubProcesses/"%(options.label,options.label))
os.system("cp ./%s_MC/SubProcesses/P0_*/nexternal.inc ./%s_ME/SubProcesses/"%(options.label,options.label))    
os.system("cp %s/lib/libpdf.a ./%s_ME/lib/"%(options.dummy_process_dir,options.label))
os.system("cp %s/src/.libs/libLHAPDF.a ./%s_ME/lib/"%(options.LHAPDF_dir,options.label))
os.system("cp -r %s/lib/Pdfdata ./%s_ME/lib/"%(options.dummy_process_dir,options.label))
os.system("cp -r %s/lib/PDFsets ./%s_ME/lib/"%(options.dummy_process_dir,options.label))
os.chdir("%s_ME/SubProcesses/"%options.label)
print "compiling reweight machinery - see  %s/%s/makefile.log for log"%(options.store_dir,options.label)
os.system("make OLP_static >  %s/%s/makefile.log"%(options.store_dir,options.label))
os.system("make check_OLP >>  %s/%s/makefile.log"%(options.store_dir,options.label))
os.chdir("../../")

runcardfile = open("%s_MC/Cards/run_card.dat"%options.label, "rt")
runcardcontent = runcardfile.read()
runcardcontent = runcardcontent.replace('nn23nlo = pdlabel', 'lhapdf = pdlabel')
runcardcontent = runcardcontent.replace('244600  = lhaid', '90500  = lhaid')
runcardcontent = runcardcontent.replace('False    = fixed_ren_scale', 'True    = fixed_ren_scale')
runcardcontent = runcardcontent.replace('False    = fixed_fac_scale', 'True    = fixed_fac_scale')
runcardcontent = runcardcontent.replace('91.118   = muR_ref_fixed', '%.4f   = muR_ref_fixed'%scale_dict[options.proc])
runcardcontent = runcardcontent.replace('91.118   = muF_ref_fixed', '%.4f   = muF_ref_fixed'%scale_dict[options.proc])
runcardcontent = runcardcontent.replace('False = store_rwgt_inf', 'True = store_rwgt_inf')
runcardcontent = runcardcontent.replace('10000 = nevents', '%i = nevents'%options.Nevents)
runcardcontent = runcardcontent.replace('HERWIG6   = parton_shower', 'PYTHIA8   = parton_shower')
runcardfile.close()
runcardfile = open("%s_MC/Cards/run_card.dat"%options.label, "wt")
runcardfile.write(runcardcontent)
runcardfile.close()

evgeneratescript = open("generateMC.sh","w")
evgeneratescript.write("./%s_MC/bin/generate_events << EOF\n"%options.label)
evgeneratescript.write("order=LO\n")
evgeneratescript.write("fixed_order=OFF\n")
evgeneratescript.write("shower=OFF\n")
evgeneratescript.write("madspin=OFF\n")
evgeneratescript.write("reweight=OFF\n")
evgeneratescript.write("done\n")
evgeneratescript.write("done\n")
evgeneratescript.write("EOF\n")
evgeneratescript.close()
os.system("chmod u+x generateMC.sh")
print "generating LO MC events - see  %s/%s/generateMC.log for log"%(options.store_dir,options.label)
os.system("./generateMC.sh >  %s/%s/generateMC.log"%(options.store_dir,options.label))

os.system("gzip -d %s_MC/Events/run_01_LO/events.lhe.gz"%options.label)
os.system("sed -i 's/    2 1.166379e-05 # Gf/    1 1.325070e+02 # aEWM1\\n    2 1.166379e-05 # Gf/g' %s_MC/Events/run_01_LO/events.lhe"%options.label)
os.system("cp %s_MC/Events/run_01_LO/events.lhe %s_ME/SubProcesses/"%(options.label,options.label))
os.chdir("%s_ME/SubProcesses/"%options.label)
print "running reweight machinery - see  %s/%s/check_OLP.log for log"%(options.store_dir,options.label)
os.system("./check_OLP >  %s/%s/check_OLP.log"%(options.store_dir,options.label))
os.chdir("../../")

#first run the shower on the LO events
####################################################
####################################################
# TMP for debug
os.system("pwd")
os.system("cp %s_MC/Cards/shower_card_default.dat %s_MC/Cards/shower_card.dat"%(options.label, options.label))
os.system("ls %s_MC/Cards/shower_card.dat"%options.label)
#os.system("sed -i 's/nevents      = -1/nevents      = %i/g' %s_MC/Cards/shower_card.dat"%(options.Nevents,options.label))
os.system("sed -i 's/nsplit_jobs  = 1/nsplit_jobs  = 8/g' %s_MC/Cards/shower_card.dat"%(options.label))
####################################################
####################################################
os.system("sed -i 's/EXTRALIBS    = stdhep Fmcfio/extralibs = pythia8 boost_iostreams z dl stdc++/g' %s_MC/Cards/shower_card.dat"%options.label)
os.system("sed -i 's&EXTRAPATHS   = ../lib&extrapaths = ../lib %s/HEPTools/pythia8//lib /usr/local/lib %s/HEPTools/zlib/lib&g' %s_MC/Cards/shower_card.dat"%(options.mg5_dir,options.mg5_dir,options.label))
print "showering LO events - see  %s/%s/showerLO.log for log"%(options.store_dir,options.label)
os.system("./%s_MC/bin/shower run_01_LO  -f >  %s/%s/showerLO.log"%(options.label,options.store_dir,options.label))
#os.system("mv %s_MC/Events/run_01_LO/events_PYTHIA8_0.hepmc.gz %s_MC/Events/run_01_LO/events_LO_PYTHIA8_0.hepmc.gz"%(options.label,options.label))
os.system("rename events_PYTHIA8_0 events_LO_PYTHIA8_0  %s_MC/Events/run_01_LO/events_PYTHIA8_0*hepmc.gz"%(options.label))

#now run the shower on the NLOEW events 
os.system("rm %s_MC/Events/run_01_LO/events*.lhe*"%options.label)
os.system("cp %s_ME/SubProcesses/events_rwgt.lhe  %s_MC/Events/run_01_LO/events.lhe"%(options.label,options.label))
print "showering reweighted events - see  %s/%s/showerNLOEW.log for log"%(options.store_dir,options.label)
os.system("./%s_MC/bin/shower run_01_LO  -f >  %s/%s/showerNLOEW.log"%(options.label,options.store_dir,options.label))
#os.system("mv %s_MC/Events/run_01_LO/events_PYTHIA8_0.hepmc.gz %s_MC/Events/run_01_LO/events_NLOEW_PYTHIA8_0.hepmc.gz"%(options.label,options.label))
os.system("rename events_PYTHIA8_0 events_NLOEW_PYTHIA8_0  %s_MC/Events/run_01_LO/events_PYTHIA8_0*hepmc.gz"%(options.label))

os.system("rm %s_MC/Events/run_01_LO/events*.lhe*"%options.label)
os.system("mv %s_MC/Events/run_01_LO/events_*.hepmc.gz %s/%s/"%(options.label,options.store_dir,options.label))
os.system("mv %s_ME/SubProcesses/events*.lhe  %s/%s/"%(options.label,options.store_dir,options.label)) 

