
### Setup and run the computation of the C1 coefficients for the parametrization of the trilinear Higgs self-coupling effects on the single-Higgs production mechanisms

## Installation

Run the install.sh script to download and install Madgraph+lhapdf+trilinear-RW
```
source install.sh
```    

After installation, and whenever starting a new session setup the environment:
```
source setup.sh
```
  
## Run an example event generation
To make sure that everithing is compiled correctly, and also to produce lib and data that will be used to run the reweight machinery, run the following example:
```
cd MG5_aMC_v2_5_5
./bin/mg5_aMC
    generate p p > t t~
    output test_ttbar
sed -i 's/nn23lo1    = pdlabel/lhapdf = pdlabel/g' test_DY/Cards/run_card.dat
sed -i 's/230000    = lhaid/90500    = lhaid/g' test_DY/Cards/run_card.dat
./test_ttbar/bin/generate_events
    shower=OFF
    detector=OFF
    analysis=OFF
    madspin=OFF
    reweight=OFF
cd ..
```
NOTE: do not delete the content of the `test_ttbar` folder because some lib and data will be re-used to run the trilinear reweight machinery.

## Run the event generation + trilinear reweight + showering&hadronization
A python script can be used to control the full process
```
python runtrilinearstudy.py --proc ZH --label myfirstZHproduction --Nevents 10000
```
NOTE: if you installed programs in a different location with respect to the `install.sh` default, you should change some default option of `runtrilinearstudy.py`. Run `python runtrilinearstudy.py --help` to have the full list of available settings.

## To do list      
* Interface for the job submission on clusters      
* Rivet classification       
* ATLAS modifications for event generation inclusive of VH(had)+VBF, and to have a floating renormalization scale      

